<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/line10">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/line10" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
            var option = {
            		   title: {
            		       text: "对数轴示例",
            		       x: "center"
            		   },
            		   tooltip: {
            		       trigger: "item",
            		       formatter: "{a} <br/>{b} : {c}"
            		   },
            		   legend: {
            		       x: 'left',
            		       data: ["2的指数", "3的指数"]
            		   },
            		   xAxis: [
            		       {
            		           type: "category",
            		           name: "x",
            		           splitLine: {show: false},
            		           data: ["一", "二", "三", "四", "五", "六", "七", "八", "九"]
            		       }
            		   ],
            		   yAxis: [
            		       {
            		           type: "log",
            		           logLabelBase: 3,
            		           splitNumber: 3,
            		           name: "y",
            		           axisLabel: {
            		                margin: '25',
            		                textStyle: {
            		                    align: 'left'
            		                }
            		           }
            		       }
            		   ],
            		    toolbox: {
            		       show: true,
            		       feature: {
            		           mark: {
            		               show: true
            		           },
            		           dataView: {
            		               show: true,
            		               readOnly: true
            		           },
            		           restore: {
            		               show: true
            		           },
            		           saveAsImage: {
            		               show: true
            		           }
            		       }
            		   },
            		   calculable: true,
            		   series: [
            		       {
            		           name: "3的指数",
            		           type: "line",
            		           data: [1, 3, 9, 27, 81, 247, 741, 2223, 6669]

            		       },
            		       {
            		           name: "2的指数",
            		           type: "line",
            		           data: [1, 2, 4, 8, 16, 32, 64, 128, 256]

            		       }
            		   ]
            		};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>